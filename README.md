SkyDragon OS Manifest
======================
The HolyDragon Project's SkyDragon OS aims to provide an enhanced operating system experience for OnePlus phones and tablets. It is open-source and built upon a fork of OmniROM, the Android-based OS. 

## Table of Contents
- [Installation Directions](#installation-directions)
- [License](#license)

## Installation Directions
This source is pre-set to build OP3/3T,5/5T,6/6T

First, clone the repository:

    git clone https://github.com/holydragonproject/android.git -b ndp

Initialize and sync the local repository using the SkyDragon trees:

    repo init -u https://gitlab.com/holydragonproject/android.git -b ndp
    repo sync -c -j# --force-sync --no-clone-bundle --no-tags

Setup build environment:

    . build/envsetup.sh

Build:

    lunch skydragon_oneplus3-userdebug
    time mka dragon


Additional steps depending on OnePlus device
======================
If building for a device with an SOC other than SD820/SD821(MSM8996).
In vendor/skydragon/sdclang/vendorsetup.sh 
change the cpu target from cortex-a57 to the correct big core cpu
Ex: if building for op6/6t, change to cortex-a75
then re-run '. build/envsetup.sh' before lunching and building device.


If building on other devices
======================
In build/make revert this commit that hardcodes using a prebuilt kernel image in root of kernel source: 
https://gitlab.com/HolyDragonProject/android_build_make/commit/92e109ef61a7e7ba874738195f03b925734c8e0f

In your device tree, you will need to make your own commit like this one below, in order for the cpuinfo qs tile to read your soc temperature. 
Not doing so will stop the rom from building. Note that thermalzone is specific to your soc:
https://gitlab.com/HolyDragonProject/android_device_oneplus_oneplus3/commit/de6c8b2637dcd360e654616bc2663c52d91a8306


## License
This project is licensed under the GNU GPLv2 and Apache License 2.0